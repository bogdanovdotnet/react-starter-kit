import React from 'react';
import Button from '../button/button'

export default class App extends React.Component {
  render() {
    return (
      <Button 
        text="Push the button!"
        onClick={this.buttonClick} />
    );
  }

  buttonClick (){
    console.log('IT WORKS!');
  }
}